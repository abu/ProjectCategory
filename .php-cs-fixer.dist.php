<?php

$config = new PhpCsFixer\Config();
$config
    ->setUsingCache(true)
    ->getFinder()
    ->in(__DIR__)
    ->exclude('node_modules')
	->exclude('.local')
	;

return $config;
