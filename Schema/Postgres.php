<?php

namespace Kanboard\Plugin\ProjectCategory\Schema;

use PDO;

const VERSION = 1;

function version_1(PDO $pdo)
{
    $pdo->exec('
        CREATE TABLE project_category (
            id SERIAL PRIMARY KEY,
            name VARCHAR(50) NOT NULL,
            UNIQUE(name)
        );
    ');

    $pdo->exec('
        CREATE TABLE proj2cat (
            proj_id INTEGER PRIMARY KEY NOT NULL,
            cat_id INTEGER NOT NULL,
            FOREIGN KEY(cat_id) REFERENCES project_category(id) ON DELETE CASCADE,
            FOREIGN KEY(proj_id) REFERENCES projects(id) ON DELETE CASCADE
        );
    ');
}
