<?php

namespace Kanboard\Plugin\ProjectCategory\Schema;

use PDO;

const VERSION = 1;

function version_1(PDO $pdo)
{
    $pdo->exec('
		CREATE TABLE IF NOT EXISTS project_category (
			id int(11) PRIMARY KEY AUTO_INCREMENT,
			name varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
            UNIQUE(name)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
	');

    $pdo->exec('
		CREATE TABLE IF NOT EXISTS proj2cat (
			proj_id int(11) PRIMARY KEY NOT NULL,
			cat_id int(11) NOT NULL,
            FOREIGN KEY(cat_id) REFERENCES project_category(id) ON DELETE CASCADE,
            FOREIGN KEY(proj_id) REFERENCES projects(id) ON DELETE CASCADE
		) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
	');
}
