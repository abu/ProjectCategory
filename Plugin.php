<?php

namespace Kanboard\Plugin\ProjectCategory;

use Kanboard\Core\Plugin\Base;
use Kanboard\Core\Translator;

class Plugin extends Base
{
    public function initialize()
    {
        // Display
        $this->template->hook->attach('template:dashboard:show:after-filter-box', 'ProjectCategory:dashboard/overview');
        // Config
        $this->template->hook->attach('template:config:sidebar', 'ProjectCategory:config/sidebar');
        // Assign
        $this->template->hook->attach('template:project:sidebar', 'ProjectCategory:project/sidebar');
        // Assets
        $this->hook->on('template:layout:js', array('template' => 'plugins/ProjectCategory/Assets/js/project-category.js'));
        $this->hook->on('template:layout:css', array('template' => 'plugins/ProjectCategory/Assets/css/project-category.css'));
    }

    public function onStartup()
    {
        // Translator::load($this->languageModel->getCurrentLanguage(), __DIR__.'/Locale');
    }

    public function getClasses()
    {
        return [
          'Plugin\ProjectCategory\Model' => [
              'ProjectCategoryModel',
          ],
        ];
    }

    public function getPluginName()
    {
        return 'ProjectCategory';
    }

    public function getPluginDescription()
    {
        return t('Separate project list by categories.');
    }

    public function getPluginAuthor()
    {
        return 'Alfred Bühler';
    }

    public function getPluginVersion()
    {
        return '0.1.0';
    }

    public function getCompatibleVersion()
    {
        return '>=1.2.20';
    }

    public function getPluginHomepage()
    {
        return 'https://codeberg.org/abu';
    }
}
