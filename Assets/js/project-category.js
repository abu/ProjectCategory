'use strict';

$(function () {
  if (new URLSearchParams(window.location.search).get('plugin') !== 'ProjectCategory') {
    return;
  }

  const app = {
    mode: 'Add',

    setAddingMode: function () {
      $('#op-button').text(this.mode = 'Add');
      $('#rm-button').attr('disabled', true);
    },

    setEditingMode: function () {
      $('#op-button').text(this.mode = 'Update');
      $('#rm-button').attr('disabled', false);
    },

    doRequest: function (action) {
      // console.log(
      //   document.getElementById('form-cat_name').value,
      //   document.getElementById('form-cat_id').name,
      // );

      const url = `/?controller=CategoryController&action=${action}&plugin=ProjectCategory`;

      $('form#cat-form').prop('action', url);
      $('form#cat-form').submit();
    },

    run: function () {
      this.setAddingMode();

      $('#op-button').click(() => {
        let cat_name = $('#form-cat_name').val();
        if (cat_name.length > 0) {
          this.doRequest(this.mode === 'Add' ? 'create' : 'update');
        }
      });

      $('#rm-button').click(() => {
        let catName = $('#form-cat_name').val();
        if (catName.length > 0) {
          this.doRequest('destroy');
        } else {
          // TODO error, rejected
        }
      });

      // $('option').click((el) => {
      $('option.procat').click((el) => {
        this.setEditingMode();

        $('#form-cat_name').val($(el.target).text());
        $('#form-cat_id').val($(el.target).val());
        // console.log(
        //   document.getElementById('form-cat_name').value,
        //   document.getElementById('form-cat_id').value,
        // );
      });

      $('#form-cat_name').keydown(() => {
        let catName = $('#form-cat_name').val();
        if (catName.length === 1) {
          this.setAddingMode();
        }
      });
    },
  };

  app.run();
})
