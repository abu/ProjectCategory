module.exports = {
    "env": {
        "browser": true,
        "es2021": true
    },
    "extends": [
        "eslint:recommended",
        "plugin:es-beautifier/standard"
    ],
    "parserOptions": {
        "ecmaVersion": 12,
        "sourceType": "script"
    },
    "plugins": [
        "es-beautifier"
    ],
    'rules': {
      'quotes': ['error', 'single'],
      'no-console': 'warn',
    },
    'globals': {
      // 'module': 'readonly',
      '$': 'readonly',
      // 't': 'readonly',
    }
};
