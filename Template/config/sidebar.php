<li <?= $this->app->checkMenuSelection('CategoryController', 'config') ?>>
    <?= $this->url->link(t('Project category'), 'CategoryController', 'config', ['plugin' => 'ProjectCategory']) ?>
</li>
