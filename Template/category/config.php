<div class="page-header">
  <h2><?= t('Project category') ?></h2>
</div>
  <div class="panel">
    <?= $this->form->label('Known categories', 'cat-select') ?>
    <div id="cat-panel">
      <select size='15' id="cat-select">
        <?php foreach ($categories as $category): ?>
          <option class='procat' value='<?=$this->text->e($category['id'])?>'><?=$this->text->e($category['name'])?></option>
        <?php endforeach ?>
      </select>
      <form method='post' id='cat-form' autocomplete='off'>
          <?= $this->form->csrf() ?>
          <?= $this->form->text('cat_name') ?>
          <?= $this->form->hidden('cat_id', array('cat_id' => '-1')) ?>
      </form>
      <div class="form-actions">
        <button id="op-button" class="btn btn-blue"><?= t('Add') ?></button>
        <button id="rm-button" class="btn btn-blue"><?= t('Delete') ?></button>
      </div>
    </div>
</div>
