<div class="page-header">
    <h2><?= t('Project category') ?></h2>
</div>
<div class="panel">
    <form method='post'
        action='<?=$this->url->href('CategoryController', 'save', array('plugin' => 'ProjectCategory'))?> ' autocomplete='off'  >
        <?= $this->form->csrf() ?>

        <?= $this->form->hidden('project_id', array('project_id' => $project['id'])) ?>
        <?= $this->form->hidden('cat_id', array('cat_id' => $assigned_cat)) ?>

        <?= $this->form->label('Select a category', 'procat-select') ?>
        <select name='procat-select'>
            <option class='procat' value='-1'><?= t('<Unassigned>')?></option>
            <?php foreach ($categories as $category): ?>
                <option class='procat' value='<?= $this->text->e($category['id']) ?>'
                    <?= $assigned_cat == $category['id'] ? 'selected' : '' ?>><?= $this->text->e($category['name']) ?></option>
            <?php endforeach ?>
        </select>

        <div class="form-actions">
          <button type='submit' class='btn btn-blue'><?= t('Save') ?></button>
        </div>
    </form>
</div>
