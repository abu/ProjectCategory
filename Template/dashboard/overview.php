<?php $projectsByCategory = $this->app->projectCategoryModel->findAllProjects($this->user->getId()); ?>
<?php if(!empty($projectsByCategory)): ?>
    <div class="table-list">
        <?= $this->render('ProjectCategory:dashboard/header') ?>
        <?php $category = '' ?>
        <?php foreach ($projectsByCategory as $pc): ?>
            <?php if ($category != $pc['category']): ?>
                <h2 class='procat-header'><?= $this->text->e($pc['category']) ?></h2>
                <?php $category = $pc['category']?>
            <?php endif ?>
            <div class="table-list-row table-border-left">
                <div>
                    <?php $project = &$pc['project'] ?>
                    <?php if ($this->user->hasProjectAccess('ProjectViewController', 'show', $project['id'])): ?>
                        <?= $this->render('project/dropdown', array('project' => $project)) ?>
                    <?php else: ?>
                        <strong><?= '#'.$project['id'] ?></strong>
                    <?php endif ?>

                    <span class="table-list-title <?= $project['is_active'] == 0 ? 'status-closed' : '' ?>">
                        <?= $this->url->link($this->text->e($project['name']), 'BoardViewController', 'show', array('project_id' => $project['id'])) ?>
                    </span>

                    <?php if ($project['is_private']): ?>
                        <i class="fa fa-lock fa-fw" title="<?= t('Private project') ?>"></i>
                    <?php endif ?>
                </div>
            </div>

        <?php endforeach ?>
    </div>
<?php else: ?>
    <div class="table-list">
        <?= $this->render('ProjectCategory:dashboard/header') ?>
        <p class="alert"><?= t('You are not a member of any project.') ?></p>
    </div>
<?php endif ?>
