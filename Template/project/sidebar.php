<li <?= $this->app->checkMenuSelection('CategoryController', 'assign') ?>>
    <?= $this->url->link(t('Project category'), 'CategoryController', 'assign', array(
        'plugin' => 'ProjectCategory',
        'project_id' => $project['id'],)) ?>
</li>
