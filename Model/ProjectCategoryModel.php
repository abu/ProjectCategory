<?php

namespace Kanboard\Plugin\ProjectCategory\Model;

use Kanboard\Core\Base;

class ProjectCategoryModel extends Base
{
    public const TABLE = 'project_category';
    public const PRO2CAT = 'proj2cat';

    /**
     * Get the full project list
     *
     * @param int $user_id  TODO
     * @return array
     */
    public function findAllProjects($user_id)
    {
        $procat = $this->db->table(self::PRO2CAT)
            ->left(self::TABLE, 't1', 'id', self::PRO2CAT, 'cat_id')
            ->asc('name')
            ->findAll();

        $result = array();

        foreach ($procat as $p) {
            $result[$p['name'] . '-' . $this->projectModel->getById($p['proj_id'])['name']] = [
                'project' => $this->projectModel->getById($p['proj_id']),
                'category' => $p['name'],
            ];
        }

        ksort($result, SORT_FLAG_CASE | SORT_STRING);
        return $result;
    }

    /**
     * Get the full category list
     *
     * @return array
     */
    public function getAll()
    {
        return $this->db->table(self::TABLE)->findAll();
    }

    /**
     * Assign product category
     *
     * @param int project_id
     * @param int category_id
     */
    public function assign(int $project_id, int $cat_id)
    {
        if ($cat_id === -1) {
            // Unassign
            $this->db->table(self::PRO2CAT)->eq('proj_id', $project_id)->remove();
        } else {
            if ($this->db->table(self::PRO2CAT)->eq('proj_id', $project_id)->exists()) {
                // update
                $this->db->table(self::PRO2CAT)->eq('proj_id', $project_id)->update(array(
                    'cat_id' => $cat_id
                ));
            } else {
                // insert
                $this->db->table(self::PRO2CAT)->insert(array(
                    'proj_id' => $project_id,
                    'cat_id' => $cat_id
                ));
            }
        }
    }

    /**
     * Get assigned product category
     *
     * @param int project_id
     * @return int category_id or -1 if unassigned
     */
    public function getCategory(int $project_id): int
    {
        $record = $this->db->table(self::PRO2CAT)->select('cat_id')->eq('proj_id', $project_id)->findOne();
        if ($record != null) {
            return intval($record['cat_id']);
        } else {
            return -1;
        }
    }

    /**
     * Create category
     *
     * @param string $cat_name
     */
    public function create(string $cat_name)
    {
        $this->db->table(self::TABLE)->insert(array('name' => $cat_name));
    }

    /**
     * Update category
     *
     * @param string $cat_id
     * @param string $cat_name
     */
    public function update(int $cat_id, string $cat_name)
    {
        $this->db->table(self::TABLE)->eq('id', $cat_id)->update(array('name' => $cat_name));
    }

    /**
     * Delete category
     *
     * @param string $cat_id
     */
    public function destroy(int $cat_id)
    {
        $this->db->table(self::TABLE)->eq('id', $cat_id)->remove();
    }
}
