
# Project Categories

**[Kanboard](https://kanboard.org) plugin for listing projects grouped by project categories.**

## Features
- Adds a new list with projects grouped by project categories. These are different from the existing categories for tasks.
- Create, edit and delete project categories.
- Assign/Unassign categories to/from a project.

## Configuration
- Available settings can be found under _Settings > Project category_.
- Assignment is located at _Configure this project > Project category_.

## Authors & Contributors
- [Alfred Bühler](https://codeberg.org/abu) - Author

## License
- This project is distributed under the [MIT License](https://choosealicense.com/licenses/mit/ "Read The MIT license")
