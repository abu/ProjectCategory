<?php

namespace Kanboard\Plugin\ProjectCategory\Controller;

use Kanboard\Controller\BaseController;

class CategoryController extends BaseController
{
    private function redirect()
    {
        $this->response->redirect($this->helper->url->to('CategoryController', 'config', array(
            'plugin' => 'ProjectCategory',
            'categories' => $this->projectCategoryModel->getAll(),
        )));
    }

    public function assign()
    {
        $values = $this->request->getValues();
        $project = $this->getProject();
        $this->response->html($this->helper->layout->project('ProjectCategory:category/assign', array(
            'title' => t('Project category'),
            'project' => $project, // required!
            'categories' => $this->projectCategoryModel->getAll(),
            'assigned_cat' => $this->projectCategoryModel->getCategory(intval($project['id'])),
        )));
    }

    public function save()
    {
        $values = $this->request->getValues();

        $proj_id = $values['project_id'];
        $cat_id = $values['procat-select'];

        $this->projectCategoryModel->assign(
            intval($proj_id),
            intval($cat_id)
        );
        $this->flash->success(t('Category successfully assigned.'));

        return $this->response->redirect($this->helper->url->to('CategoryController', 'assign', array(
            'project_id' => $proj_id,
            'plugin' => 'ProjectCategory',
        )));
    }

    public function config()
    {
        $this->response->html($this->helper->layout->config('ProjectCategory:category/config', array(
            'title' => t('Settings').' &gt; '.t('Project categories'),
            'categories' => $this->projectCategoryModel->getAll(),
        )));
    }

    public function create()
    {
        $values = $this->request->getValues();
        $this->projectCategoryModel->create($values['cat_name']);

        $this->flash->success(t('Category successfully added.'));
        $this->redirect();
    }

    public function update()
    {
        $values = $this->request->getValues();
        $this->projectCategoryModel->update(intval($values['cat_id']), $values['cat_name']);

        $this->flash->success(t('Category successfully updated.'));
        $this->redirect();
    }

    public function destroy()
    {
        $values = $this->request->getValues();
        $this->projectCategoryModel->destroy(intval($values['cat_id']));

        $this->flash->success(t('Category successfully deleted.'));
        $this->redirect();
    }
}
